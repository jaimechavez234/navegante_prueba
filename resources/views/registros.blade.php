@extends('bases.base')

@section('content')
<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar" data-sidebarbg="skin5">
<!-- Sidebar scroll-->
<div class="scroll-sidebar">
    <!-- Sidebar navigation-->
    <nav class="sidebar-nav">
        <ul id="sidebarnav" class="p-t-30">
        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/" aria-expanded="false"><i class="mdi mdi-account-check"></i><span class="hide-menu">Agregar</span></a></li>
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/registros" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Mostrar Registros</span></a></li>
            
        </ul>
    </nav>
    <!-- End Sidebar navigation -->
</div>
<!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
    @foreach($Registros as $dato)
        <div class="col-md-6">
        <div class="card">
            <div class="comment-widgets scrollable">
                <div class="d-flex flex-row comment-row m-t-0">
                    <div class="p-2"><img src="assets/images/users/1.jpg" alt="user" width="50" class="rounded-circle"></div>
                    <div class="comment-text w-100">
                        <h2 class="font-medium">{{ $dato->firstName }} {{ $dato->middleName }} {{ $dato->lastName }}</h2>
                        <h5 class="font-medium">{{ $dato->email }} - {{ $dato->mobileNumber }} - {{ $dato->phoneNumber }}</h5>
                        <span class="m-b-2 d-block">Gender: {{ $dato->gender }}</span>
                        <span class="m-b-2 d-block">Address 1: {{ $dato->address1 }}</span>
                        <span class="m-b-2 d-block">Address 2: {{ $dato->address2 }}</span>
                        <span class="m-b-2 d-block">Zip Code: {{ $dato->zipCode }}</span>
                        <div class="comment-footer">
                            <span class="text-muted float-right">Birth Date: {{ $dato->birthDate }} - {{ $dato->country }} - {{ $dato->city }} - {{ $dato->state }}</span> 
                            <form action="/registro/{{$dato->id}}" method="post">
                            {{ csrf_field() }}
                                <button type="submit" class="btn btn-cyan btn-sm">Edit</button>
                            </form>
                            
                            <form action="/borrar/{{$dato->id}}" method="post">
                                {{ csrf_field() }}
                                
                                <button type="submit" onclick="return confirm('¿Deesea borrar el registro?')" class="btn btn-danger btn-sm">Delete</button>
                            </form>
                        </div>
                    </div>
                </div>  
            </div>
            </div>
        </div>
    @endforeach
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@stop
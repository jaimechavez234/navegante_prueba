@extends('bases.base')

@section('content')
<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar" data-sidebarbg="skin5">
<!-- Sidebar scroll-->
<div class="scroll-sidebar">
    <!-- Sidebar navigation-->
    <nav class="sidebar-nav">
        <ul id="sidebarnav" class="p-t-30">
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/" aria-expanded="false"><i class="mdi mdi-account-check"></i><span class="hide-menu">Agregar</span></a></li>
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/registros" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Mostrar Registros</span></a></li>
        </ul>
    </nav>
    <!-- End Sidebar navigation -->
</div>
<!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="card">
        <div class="card-body wizard-content">
            <h4 class="card-title">Agregar Nuevo Registro</h4>
            <h6 class="card-subtitle"></h6>
            <form id="example-form" action="{{ url('/agregar') }}" enctype="multipart/form-data" method="post" class="m-t-40">
            {{ csrf_field() }}
                <div>
                    <h3>Datos Personales</h3>
                    <section>
                        <div class="form-row">
                            <div class="col-md-4">
                                <label for="name">First Name*</label>
                                <input id="name" name="firstName" type="text" class="required form-control">
                            </div>
                            <div class="col-md-4">
                                <label for="middleName">Middle Name*</label>
                                <input id="middleName" name="middleName" type="text" class="required form-control">
                            </div>
                            <div class="col-md-4">
                                <label for="lastName">Last Name*</label>
                                <input id="lastName" name="lastName" type="text" class="required form-control">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-4">
                                <label for="birthDate">Birth Date*</label>
                                <input type="date" id="birthDate" name="birthDate" class="required form-control">
                            </div>
                            <div class="col-md-8">
                                <label for="gender">Gender*</label>
                                <select id="gender" name="gender" class="form-control" class="required form-control">
                                    <option selected>Choose...</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>
                        </div>
                        <!-- <label for="email">Correo *</label>
                        <input id="email" name="email" type="text" class="required email form-control">
                        <label for="address">Dirección *</label>
                        <input id="address" name="address" type="text" class=" form-control"> -->
                        <p>(*) Obligatorio</p>
                    </section>
                    <h3>Address</h3>
                    <section>
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="address1">Street Address*</label>
                                <input id="address1" name="address1" type="text" class="required form-control">
                            </div>
                            <div class="col-md-6">
                            <label for="addres2">Street Address Line 2</label>
                                <input id="address2" name="address2" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-4">
                                <label for="city">City*</label>
                                <input id="city" name="city" type="text" class="required form-control">
                            </div>
                            <div class="col-md-4">
                                <label for="state">State/Province*</label>
                                <input id="state" name="state" type="text" class="required form-control">
                            </div>
                            <div class="col-md-4">
                                <label for="zipCode">Postal/ZipCode*</label>
                                <input id="zipCode" name="zipCode" type="text" class="required form-control">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="country">Country*</label>
                                <input id="country" name="country" type="text" class="required form-control">
                            </div>
                        </div>
                        <p>(*) Obligatorio</p>
                    </section>
                    <h3>Contact</h3>
                    <section>
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="email">Student E-mail*</label>
                                <input id="email" name="email" type="email" class="required form-control">
                            </div>
                            <div class="col-md-6">
                                <label for="mobileNumber">Mobile Number*</label>
                                <input id="mobileNumber" name="mobileNumber" type="number" class="required form-control">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="phoneNumber">Phone Number</label>
                                <input id="phoneNumber" name="phoneNumber" type="number" class="form-control">
                            </div>
                            <div class="col-md-6">
                                <label for="workNumber">Work Number</label>
                                <input id="workNumber" name="workNumber" type="number" class="form-control">
                            </div>
                        </div>
                    </section>
                    <h3>Additional Information</h3>
                    <section>
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="company">Company*</label>
                                <input id="company" name="company" type="text" class="required form-control">
                            </div>
                            <div class="col-md-6">
                                <label for="courses">Courses*</label>
                                <select id="courses" name="courses" class="form-control" class="required form-control">
                                    <option selected>Choose...</option>
                                    <option value="Windows 8">Windows 8</option>
                                    <option value="Introduction to Linux">Introduction to Linux</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-control">
                            <label for="addirionalComments">Aditional Comments*</label>
                            <textarea id="addirionalComments" name="addirionalComments" rows="5" class="required form-control"></textarea>
                        </div>
                    </section>
                    <h3>Finish</h3>
                    <section>
                        <input id="acceptTerms" name="acceptTerms" type="checkbox" class="required">
                        <label for="acceptTerms">I agree with the Terms and Conditions.</label>
                    </section>
                </div>
            </form>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@stop

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'RegistrosController@index');
Route::get('/registros', 'RegistrosController@mostrar');

Route::post('/agregar', 'RegistrosController@store');
Route::post('/borrar/{id}', 'RegistrosController@destroy');
Route::post('/registro/{id}', 'RegistrosController@edit');
Route::post('/editRegistro/{id}', 'RegistrosController@update');

Route::name('print')->get('/imprimir', 'RegistrosController@imprimir');
<?php

namespace App\Http\Controllers;

use App\Registros;
use Illuminate\Http\Request;

class RegistrosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view("welcome");
    }

    public function mostrar(){
        $datos['Registros']=Registros::all();
        return view("registros", $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $datos=request()->all();
        $datos=request()->except('_token', 'acceptTerms');
        Registros::insert($datos);
        return redirect('registros');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Registros  $registros
     * @return \Illuminate\Http\Response
     */
    public function show(Registros $registros)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Registros  $registros
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $registro=Registros::findOrFail($id);
        return view('edit', compact('registro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Registros  $registros
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $datos=request()->except(['_token', 'acceptTerms']);
        Registros::where('id', '=', $id)->update($datos);
        return redirect('registros');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Registros  $registros
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Registros::destroy($id);
        return redirect('registros');
    }

    public function imprimir(){
        $datos['registros']=Registros::all();
        $pdf = \PDF::loadView('tabla', $datos);
        return $pdf->download('tabla_navegante.pdf');
   }
}
